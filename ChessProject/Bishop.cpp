#include "Bishop.h"
#include "Board.h"

/*
* This is the constructor of the Bishop class
* param row: The row of the piece
* param column: The column of the piece
* param isWhite: Is the color of the piece white
*/
Bishop::Bishop(int row, int column, bool isWhite) : Piece(row, column, isWhite)
{
}

/*
* This function checks if the given move is a valid move for the Bishop
* param row: The row of the target cell
* param column: The column of the target cell
* param board: The board that contains the pieces
* return: The code if the move was valid or invalid
*/
int Bishop::isValidMove(int row, int column, Board *board)
{
	//check if the new position of the bishop is one of the diagnoals
	if (abs((double)(_row - row)) == abs((double)(_column - column)))
	{
		//Add increament for check the next cells for Row and Colunm , in this case x and y 
		int xIncrement = (row - _row) / (abs(row - _row));
		int yIncrement = (column - _column) / (abs(column - _column));

		//check with loop all the cells 
		for (int i = 1; i < abs(_row - row); i++)
		{
			//check if the cell is empty or not 
			if (!board->isEmpty(_row + xIncrement * i , _column + yIncrement * i) )
				return INVALID_MOVE_OF_PIECE;

		}
		return PIECE_VALID_MOVE;
	}
	else
	{
		return INVALID_MOVE_OF_PIECE;
	}
}