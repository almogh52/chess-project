#pragma once
#include <cmath>
#include "Piece.h"
#include "ErrorCodes.h"

class Bishop: public Piece
{
public:
	Bishop(int row, int column, bool isWhite);

	virtual int isValidMove(int row, int column, Board *board);
};

