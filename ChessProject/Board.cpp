#include "Board.h"

/*
* This is the constructor of the Board class
* param initialPieces: The starting pieces in the board
*/
Board::Board(std::string initialPieces)
{
    int row = 1, column = 1;
	bool isWhite = false;

    // Go through the key pieces in the initial pieces and add each one to the board pieces
    for (char pieceKey : initialPieces)
    {
		// Check if the letter is a captial letter, it means it's a white piece
		isWhite = isupper(pieceKey);

		// For each piece key add it's matching Piece object to the pieces object
		switch (pieceKey)
		{
		case 'r':
		case 'R':
			pieces.push_back(new Rook(row, column, isWhite));
			break;

		case 'k':
			_blackKing = new King(row, column, false);
			pieces.push_back(_blackKing);
			break;

		case 'K':
			_whiteKing = new King(row, column, true);
			pieces.push_back(_whiteKing);
			break;

		case 'n':
		case 'N':
			pieces.push_back(new Knight(row, column, isWhite));
			break;

		case 'b':
		case 'B':
			pieces.push_back(new Bishop(row, column, isWhite));
			break;

		case 'q':
		case 'Q':
			pieces.push_back(new Queen(row, column, isWhite));
			break;

		case 'p':
		case 'P':
			pieces.push_back(new Pawn(row, column, isWhite));
			break;
		}

		// Move to the next column
		column++;

		// If we reached after the end of the board, reset col and move to next row
		if (column == BOARD_SIZE + 1)
		{
			column = 1;
			row++;
		}
    }
}

/*
* This is the deconstructor of the Board class. It frees all the pieces in the vector and clears the vector
*/
Board::~Board()
{
	for (int i = 0; i< pieces.size(); i++) //Delete Each piece 
	{
		delete (pieces[i]);
	}
	pieces.clear();//clear the pieces
}

/*
* This function moves a given piece to a given cell using a move string
* param moveString: The string should contain source cell and dest cell
* param isWhiteTurn: Is the current turn of the white player
* return: The result code of the mode, can be valid or invalid
*/
int Board::movePiece(std::string moveString , bool isWhiteTurn)
{
    int status = -1, moveCode = 0, dstRow = 0, dstCol = 0, srcRow = 0, srcCol = 0;
    Piece *srcPiece, *dstPiece;

    // Get the dest row and col on the board
    dstRow = moveString[3] - '0';
    dstCol = convertAlphaToIndex(moveString[2]);

    // Check if the indexes are in valid range
    if (!isValidIndexMove(moveString))
    {
		status = INVALID_MOVE_INVALID_BOARD_INDEXES;
    }

    // Try and get the pieces of the src cell and the dst cell
    srcPiece = getPiece(moveString.substr(0, 2));
    dstPiece = getPiece(moveString.substr(2, 2));

    // If the status is empty and the source piece doesn't exist or it's not the player's
    if (status == -1 && (!srcPiece || srcPiece->isWhite() != isWhiteTurn))
    {
		status = INVALID_MOVE_NO_CURRENT_PLAYER_PIECE_ON_SOURCE_TILE;
    }
    
    // If the status is empty and the piece in the dst cell exists and the color of it is in the color of the player
    if (status == -1 && dstPiece && dstPiece->isWhite() == isWhiteTurn)
    {
		status = INVALID_MOVE_TILE_IS_TAKEN_BY_YOUR_OWN_PIECE;
    }

    // If the status is empty and the src piece is the same as the dst piece
    if (status == -1 && srcPiece == dstPiece)
    {
		status = INVALID_MOVE_SAME_SRC_TILE_AND_DST_TILE;
    }

	// If the source piece isn't an empty piece and it's the player's piece, try to move it
	if (srcPiece && srcPiece->isWhite() == isWhiteTurn)
	{
		// Save the row and col before moving
		srcCol = srcPiece->getColumn();
		srcRow = srcPiece->getRow();

		// Try to move the piece to the target cell
		moveCode = srcPiece->move(dstRow, dstCol, this);

		// If this move was an en-passent move, set move as valid
		if (checkEnPassent(srcPiece, dstRow, dstCol))
		{
			// Set move as valid
			moveCode = PIECE_VALID_MOVE;

			// Force move the piece to the pos
			srcPiece->forceMove(dstRow, dstCol);

			// Set dst piece as the en-passent killed piece
			dstPiece = _enPassent;

			// Remove the en-passent candidate
			_enPassent = NULL;
		}

		// Try and move the source piece, if it's valid set status as valid
		if (status == -1 && moveCode == PIECE_VALID_MOVE)
		{
			// If there is a dest piece, move it outside the board's borders to not interfere with the check and mate check
			if (dstPiece)
			{
				dstPiece->forceMove(0, 0);
			}

			// Check for check in the beginning
			if (isWhiteTurn)
			{
				// If there is now a check on the current player's king, send invalid move
				if (checkForCheck(_whiteKing))
				{
					status = INVALID_MOVE_CHECK_WILL_BE_CAUSED_BY_MOVE;
				}
				// If there is a check on the opponnet's king
				else if (checkForCheck(_blackKing)) {
					// Check if the opponnet king is in mate
					if (checkForMate(_blackKing))
					{
						status = VALID_MOVE_WIN;
					}
					else {
						status = VALID_MOVE_CHECK_ON_OPPONNET;
					}
				}
				else {
					status = VALID_MOVE;
				}
			}
			else {
				// If there is now a check on the current player's king, send invalid move
				if (checkForCheck(_blackKing))
				{
					status = INVALID_MOVE_CHECK_WILL_BE_CAUSED_BY_MOVE;
				}
				// If there is a check on the opponnet's king
				else if (checkForCheck(_whiteKing)) {
					// Check if the opponnet king is in mate
					if (checkForMate(_whiteKing))
					{
						status = VALID_MOVE_WIN;
					}
					else {
						status = VALID_MOVE_CHECK_ON_OPPONNET;
					}
				}
				else {
					status = VALID_MOVE;
				}
			}

			// Return the dst piece back to it's location after moving it for the check and mate checks
			if (dstPiece)
			{
				dstPiece->forceMove(dstRow, dstCol);
			}
		}
		else if (status == -1) {
			status = INVALID_MOVE_OF_PIECE;
		}

		// If the player made on himself check, undo the move by force moving the piece back to it's piece
		if (status == INVALID_MOVE_CHECK_WILL_BE_CAUSED_BY_MOVE)
		{
			srcPiece->forceMove(srcRow, srcCol);
		}
	}

	// If the move was valid
	if (status == VALID_MOVE || status == VALID_MOVE_CHECK_ON_OPPONNET || status == VALID_MOVE_WIN)
	{
		// If there was a piece in the dst cell, remove it
		if (dstPiece != NULL)
		{
			// Remove the dst piece from the pieces vector
			pieces.erase(std::remove(pieces.begin(), pieces.end(), dstPiece));

			// Free it's memory
			delete dstPiece;
		}

		// If the previous move was a candidate for en-passent but wasn't used, remove it
		if (_enPassent != NULL)
		{
			_enPassent = NULL;
		}

		// If the piece is a Pawn and it just used it 2 forward movement, it is a candidate for being killed using en-passent
		if (dynamic_cast<Pawn *>(srcPiece) != NULL && (abs(srcRow - srcPiece->getRow())) == EN_PASSENT_DISTANCE)
		{
			_enPassent = (Pawn *)srcPiece;
		}

		// Call the after move function for the piece
		srcPiece->afterMove();
	}

    return status;
}

/*
* This function checks if the move of the pawn is en-passent
* param srcPiece: The piece that needs to be moved
* param dstRow: The target cell's row
* param dstCol: The target cell's col
* return: True if the move was en-passent and false otherwise
*/
bool Board::checkEnPassent(Piece *srcPiece, int dstRow, int dstCol) const
{
	return (_enPassent != NULL && // If the previous move was a canidate for en-passent
		dynamic_cast<Pawn *>(srcPiece) != NULL && // The current piece is a pawn
		((srcPiece->isWhite() && srcPiece->getRow() == EN_PASSENT_WHITE_SIDE_ROW) || // The row of the piece is 4 on white side
		(!srcPiece->isWhite() && srcPiece->getRow() == EN_PASSENT_BLACK_SIDE_ROW)) && // Or 5 on black side
		dstCol == _enPassent->getColumn() && // The dst col is the same as the canidate's col
		(abs(dstRow - _enPassent->getRow()) == 1)); // The distance between the rows is 1
}

/*
* This function gets a piece from the vector using it's pos
* param pieceString: The string should contain the source cell on the chess board
* return: The pointer to the piece if found or NULL else
*/
Piece * Board::getPiece(std::string pieceString) const
{ 
	int row = 0, column = 0;

	// Get the row by taking the number of the piece and converting it to int
	row = pieceString[1] - '0';

	// Get the column by converting the letter of the piece cell
	column = convertAlphaToIndex(pieceString[0]);

	for (int i = 0; i < pieces.size(); i++)//Check each pieces
	{
		if (pieces[i]->getRow() == row && pieces[i]->getColumn() == column)//check each piece and check if the rows and the columns are equales to the parameters we recieve
		{
			return pieces[i];//return the correct piece 
		}
	}

	return nullptr; // if he didnt find it return null pointer
}

/*
* This function checks if a given position is empty on the board
* param row: The row of the cell
* param col: The column of the cell
* return: True if the cell is empty or false otherwise
*/
bool Board::isEmpty(int row, int col) const
{
	// Get the position of the piece in string
	std::string piecePos = convertPosToString(row, col);

	// If the piece is invalid, return not empty
	if (piecePos == "")
	{
		return false;
	}

    // Try and get the piece
    return !getPiece(piecePos);
}

/*
* This function checks if there is a check on a king
* param king: The king to check if there is a check on
* return: True if there is a check on the king or false otherwise
*/
bool Board::checkForCheck(Piece * king) const
{
    bool check = false;

    // For each piece check if he can kill the king
    for (Piece *piece : pieces)
    {
		// Check if the piece can kill the king
		if (piece->isWhite() != king->isWhite() && piece->canKillKing((Board *)this, king))
		{
			check = true;
		}
    }

    return check;
}

/*
* This function checks if there is a mate on a king
* param king: The king to check if there is a mate on
* return: True if there is a mate on the king or false otherwise
*/
bool Board::checkForMate(Piece * king) const
{
	bool mate = true;
	int row = king->getRow(), column = king->getColumn();

	// Try to escape mate by moving to all directions possible
	if (canEscapeCheck(king, row + 1, column) ||
		canEscapeCheck(king, row - 1, column) ||
		canEscapeCheck(king, row, column + 1) ||
		canEscapeCheck(king, row, column - 1) ||
		canEscapeCheck(king, row + 1, column + 1) ||
		canEscapeCheck(king, row - 1, column + 1) ||
		canEscapeCheck(king, row + 1, column - 1) ||
		canEscapeCheck(king, row - 1, column - 1))
	{
		mate = false;
	}

	// Return the king to it's original pos
	king->forceMove(row, column);

	return mate;
}

/*
* This function checks if a king can escape check by moving to a pos
* param king: The king to check if he can escape from check
* param row: The target cell's row
* param col: The target cell's row
* return: True if the king can escape check or false otherwise
*/
bool Board::canEscapeCheck(Piece * king, int row, int col) const
{
	Piece *targetCell;
	std::string piecePos = convertPosToString(row, col);

	// If the target piece is invalid, ignore the check
	if (piecePos == "")
	{
		return false;
	}

	// Try and get the piece in the target pos of the king, if it's in the same color of the king, return false
	targetCell = getPiece(piecePos);
	if (targetCell && targetCell->isWhite() == king->isWhite())
	{
		return false;
	}
	
	// Force move the king to that position
	king->forceMove(row, col);

	// Check for check, if there isn't a check return true, since the king escaped
	return !checkForCheck(king);
}

/*
* This function converts an alpha character from the columns on the board to an index starting with 1
* param alpha: The alphabatic character
* return: The index of the alpha on the board
*/
int Board::convertAlphaToIndex(char alpha) const
{
	int index;
	index = (alpha - 'a' ) + 1; // convert the char index in the chess table to nomerical index 
	return index;//return the index 
}

/*
* This function converts a position of a cell to a piece string
* param row: The row of the cell
* param col: The col of the cell
* return: The piece string that contains the row and col on the board
*/
std::string Board::convertPosToString(int row, int col) const
{
	std::string piecePos = "";

	// If the pos is invalid, return empty string
	if (row < 1 ||
		row > BOARD_SIZE ||
		col < 1 ||
		col > BOARD_SIZE)
	{
		return "";
	}

	// Set the piece pos in ascii
	piecePos += col + 'a' - 1;
	piecePos += row + '1' - 1;

	return piecePos;
}

/*
* This function checks if a move string has a valid indexes
* param moveString: The move string to be checked
* return: True if the indexes are valid or false otherwise
*/
bool Board::isValidIndexMove(std::string moveString) const
{
    int rowSrc = 0,
	colSrc = 0,
	rowDst = 0,
	colDst = 0;

    // Get the rows of the src cell and dst cell
    rowSrc = moveString[1] - '0';
    rowDst = moveString[3] - '0';

    // Gets the columns of the src and dst cell by converting it's alpha letter 
    colSrc = convertAlphaToIndex(moveString[0]);
    colDst = convertAlphaToIndex(moveString[2]);

    // Check if all the indexes are in the range of the board
    return (rowSrc >= 1 && rowSrc <= BOARD_SIZE) &&
		   (rowDst >= 1 && rowDst <= BOARD_SIZE) &&
		   (colSrc >= 1 && colSrc <= BOARD_SIZE) &&
		   (colDst >= 1 && colDst <= BOARD_SIZE);
}