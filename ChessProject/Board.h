#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <ctype.h>
#include "Piece.h"
#include "Rook.h"
#include "Knight.h"
#include "King.h"
#include "Bishop.h"
#include "Queen.h"
#include "Pawn.h"

#define BOARD_SIZE 8

#define EN_PASSENT_DISTANCE 2
#define EN_PASSENT_WHITE_SIDE_ROW 4
#define EN_PASSENT_BLACK_SIDE_ROW 5

class Board
{
public:
	Board(std::string initialPieces);
	~Board();

	int movePiece(std::string moveString , bool isWhiteTurn);
	Piece* getPiece(std::string pieceString) const;
	bool isEmpty(int row, int col) const;

private:
	bool checkEnPassent(Piece * srcPiece, int dstRow, int dstCol) const;
	bool checkForCheck(Piece *king) const;
	bool checkForMate(Piece *king) const;
	bool canEscapeCheck(Piece *king, int row, int col) const;
	int convertAlphaToIndex(char alpha) const;
	std::string convertPosToString(int row, int col) const;
	bool isValidIndexMove(std::string moveString) const;

	std::vector<Piece *> pieces;
	King *_whiteKing;
	King *_blackKing;
	Pawn *_enPassent;
};

