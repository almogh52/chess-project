#include "GameManager.h"

/*
* This is the constructor of the GameManager class
* param initialPieces: The starting pieces in the board
*/
GameManager::GameManager(std::string initialPieces)
{
    // Create the board using the initial pieces
    _board = new Board(initialPieces);

    // If the first player is starting is the white one
    _isWhiteTurn = initialPieces[64] == '0';
}

/*
* This is the deconstructor of the GameManager class
*/
GameManager::~GameManager()
{
	// Delete the board which will delete all the pieces
	delete _board;
}

/*
* This function handles a move command from the frontend and returns the result in a string
* param moveString: The move command
* param resultString: The string that will hold the result in it
* return: None
*/
void GameManager::handleFrontendMove(std::string moveString, char *resultString)
{
    // Try and move the piece in the board
    int status = _board->movePiece(moveString, _isWhiteTurn);

    // If the move was valid, flip the turn
    if (status == VALID_MOVE || status == VALID_MOVE_CHECK_ON_OPPONNET)
    {
		// Flip the turn
		_isWhiteTurn = !_isWhiteTurn;
    }

    // Set the result status in the result string and set NULL after
    resultString[0] = status + '0';
    resultString[1] = NULL;
}

