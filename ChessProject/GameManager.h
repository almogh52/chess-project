#pragma once
#include <string>
#include "Board.h"
#include "ErrorCodes.h"

class GameManager
{
public:
    GameManager(std::string initialPieces);
	~GameManager();

    void handleFrontendMove(std::string moveString, char *resultString);

private:
    bool _isWhiteTurn;
    Board *_board;
};

