#include "King.h"
#include "Board.h"

/*
* This is the constructor of the King class
* param row: The row of the piece
* param column: The column of the piece
* param isWhite: Is the color of the piece white
*/
King::King(int row, int column, int isWhite) : Piece(row, column, isWhite)
{
}

/*
* This function checks if the given move is a valid move for the King
* param row: The row of the target cell
* param column: The column of the target cell
* param board: The board that contains the pieces
* return: The code if the move was valid or invalid
*/
int King::isValidMove(int row, int column, Board *board)
{
    double distanceRow = pow(_row - row, 2);
	double distanceCol = pow(_column - column, 2);

    // If the king's distance from the it's new position is 1 tier then return valid move
    if (sqrt(abs(distanceRow - distanceCol)) == 1 || (abs(distanceRow) == 1 && abs(distanceRow) == 1))
    {
		return PIECE_VALID_MOVE;
    }
    else {
		return INVALID_MOVE_OF_PIECE;
    }

    return 0;
}

/*
* This function checks if the King can kill another king
* param king: The king to be killed
* param board: The board that contains the pieces
* return: True if this king can be killed by this piece
*/
bool King::canKillKing(Board * board, Piece * king)
{
	// If the king to be checked, is this object, ignore and return false
	if (king == this)
	{
		return false;
	}

	// Check if this king can kill the other king by moving to him with 1 move
    return isValidMove(king->getRow(), king->getColumn(), board) == PIECE_VALID_MOVE;
}
