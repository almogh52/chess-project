#pragma once
#include "Piece.h"
#include "ErrorCodes.h"
#include <cmath>

class King : public Piece
{
public:
    King(int row, int column, int isWhite);
    
    virtual bool canKillKing(Board *board, Piece *king);
	virtual int isValidMove(int row, int column, Board *board);
};

