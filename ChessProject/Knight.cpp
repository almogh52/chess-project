#include "Knight.h"

/*
* This is the constructor of the Knight class
* param row: The row of the piece
* param column: The column of the piece
* param isWhite: Is the color of the piece white
*/
Knight::Knight(int row, int column, bool isWhite) : Piece(row, column, isWhite)
{
}

/*
* This function checks if the given move is a valid move for the Knight
* param row: The row of the target cell
* param column: The column of the target cell
* param board: The board that contains the pieces
* return: The code if the move was valid or invalid
*/
int Knight::isValidMove(int row, int column, Board *board)
{
	// Check if the dst pos of the knight is a valid L move
	if ((row + 2 == this->_row && column + 1 == this->_column)||
		(row + 2 == this->_row && column - 1 == this->_column)||
		(row - 2 == this->_row && column + 1 == this->_column)||
		(row - 2 == this->_row && column - 1 == this->_column)||
		(row + 1 == this->_row && column + 2 == this->_column)||
		(row + 1 == this->_row && column - 2 == this->_column)||
		(row - 1 == this->_row && column + 2 == this->_column)||
		(row - 1 == this->_row && column - 2 == this->_column))
	{
		return PIECE_VALID_MOVE;
	}
	else
	{
		return INVALID_MOVE_OF_PIECE;
	}
}
