#pragma once
#include "Piece.h"
#include "ErrorCodes.h"

class Knight: public Piece
{
public:
	Knight(int row, int column, bool isWhite);

	virtual int isValidMove(int row, int column, Board *board);
};

