#include "Pawn.h"
#include "Board.h"

/*
* This is the constructor of the Pawn class
* param row: The row of the piece
* param column: The column of the piece
* param isWhite: Is the color of the piece white
*/
Pawn::Pawn(int row, int column, bool isWhite) : Piece(row, column, isWhite), _firstMove(true)
{
}

/*
* This function checks if the given move is a valid move for the Pawn
* param row: The row of the target cell
* param column: The column of the target cell
* param board: The board that contains the pieces
* return: The code if the move was valid or invalid
*/
int Pawn::isValidMove(int row, int column, Board * board)
{
	bool valid = false;

	// If the piece is a white piece, it means it needs to go up (-1 diff) and if it's black it needs to go down (1 diff)
	int moveDiff = isWhite() ? -1 : 1;

	// If the pawn moved two rows on first move, one row on regular move, or jump to his side to kill another piece, return valid move
	if ((_firstMove && column == _column && row == _row + moveDiff * 2) ||
		(column == _column && row == _row + moveDiff * 1) ||
		(column == _column - 1 && row == _row + moveDiff * 1 && !board->isEmpty(row, column)) ||
		(column == _column + 1 && row == _row + moveDiff * 1 && !board->isEmpty(row, column)))
	{
		valid = true;
	}

	return valid ? PIECE_VALID_MOVE : INVALID_MOVE_OF_PIECE;
}

/*
* This function is a post-move function that will be called after the pawn has been moved
* return: None
*/
void Pawn::afterMove()
{
	// If it's the pawn first move, set first move boolean indicator as false
	if (_firstMove)
	{
		_firstMove = false;
	}
}
