#pragma once
#include "Piece.h"

class Pawn :
	public Piece
{
public:
	Pawn(int row, int column, bool isWhite);
	
	virtual void afterMove();
	virtual int isValidMove(int row, int column, Board *board);

private:
	bool _firstMove;
};

