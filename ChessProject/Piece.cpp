#include "Piece.h"
#include "Board.h"

/*
* This is the constructor of the Piece class
* param row: The row of the piece
* param column: The column of the piece
* param isWhite: Is the color of the piece white
*/
Piece::Piece(int row, int column, bool isWhite) : _row(row), _column(column), _isWhite(isWhite)
{
}

/*
* This function force moves the piece without any checks
* param row: The target row
* param column: The target column
* return: None
*/
void Piece::forceMove(int row, int column)
{
	// Force move the piece without any checking
	_row = row;
	_column = column;
}

/*
* This function checks if the Piece can kill a king
* param king: The king to be killed
* param board: The board that contains the pieces
* return: True if this king can be killed by this piece
*/
bool Piece::canKillKing(Board * board, Piece * king)
{
	// Check if it's a valid move for the piece to move to the king's pos and kill him, if it's a valid him return true
	return isValidMove(king->getRow(), king->getColumn(), board) == PIECE_VALID_MOVE;
}

/*
* This function is a post-move function that will be called after the piece has been moved
* return: None
*/
void Piece::afterMove()
{
}

/*
* This function tries to move the piece to the target cell
* param row: The row of the target cell
* param column: The column of the target cell
* param board: The board that contains the pieces
* return: The code if the move was valid or invalid
*/
int Piece::move(int row, int column, Board *board)
{
	int status = this->isValidMove(row, column, board);
	if (status == PIECE_VALID_MOVE)
	{
	    _row = row;
	    _column = column;
	}

	return status;
}

// Getters
int Piece::getRow() const
{
	return _row;
}

int Piece::getColumn() const
{
	return _column;
}

bool Piece::isWhite() const
{
	return _isWhite;
}

