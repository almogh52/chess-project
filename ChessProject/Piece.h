#pragma once
#include "ErrorCodes.h"

class Board;
class Piece
{
public:
	Piece(int row, int column, bool isWhite);

	int getRow() const;
	int getColumn() const;
	bool isWhite() const;

	void forceMove(int row, int column);
	int move(int row, int column, Board *board);

	virtual bool canKillKing(Board *board, Piece *king);
	virtual void afterMove();
	virtual int isValidMove(int row, int column, Board *board) = 0;

protected:
	int _row;
	int _column;

private:
	bool _isWhite;

};

