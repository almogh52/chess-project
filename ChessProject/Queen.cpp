#include "Queen.h"

/*
* This is the constructor of the Queen class
* param row: The row of the piece
* param column: The column of the piece
* param isWhite: Is the color of the piece white
*/
Queen::Queen(int row, int column, bool isWhite) : Piece(row, column, isWhite), _rook(row, column, isWhite) , _bishop(row, column, isWhite)
{
}

/*
* This function checks if the given move is a valid move for the Queen
* param row: The row of the target cell
* param column: The column of the target cell
* param board: The board that contains the pieces
* return: The code if the move was valid or invalid
*/
int Queen::isValidMove(int row, int column, Board *board)
{
	// Force move the rook and column to the queen's position, in order to use their checks
	_rook.forceMove(_row, _column);
	_bishop.forceMove(_row, _column);

	//check if rook is valid or the bishop is valid
	if (_rook.isValidMove(row , column , board) == PIECE_VALID_MOVE || _bishop.isValidMove(row, column, board) == PIECE_VALID_MOVE)
	{
		 return PIECE_VALID_MOVE; 
	}
	return INVALID_MOVE_OF_PIECE;
}
