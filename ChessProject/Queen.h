#pragma once
#include "Piece.h"
#include "Rook.h"
#include "Bishop.h"

class Queen :
	public Piece
{
public:
	Queen(int row, int column, bool isWhite);

	virtual int isValidMove(int row, int column, Board *board);

private:
	Rook _rook;
	Bishop _bishop; 
};

