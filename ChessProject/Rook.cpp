#include "Rook.h"
#include "Board.h"

/*
* This is the constructor of the Rook class
* param row: The row of the piece
* param column: The column of the piece
* param isWhite: Is the color of the piece white
*/
Rook::Rook(int row, int column, bool isWhite) : Piece(row, column, isWhite)
{
}

/*
* This function checks if the given move is a valid move for the Rook
* param row: The row of the target cell
* param column: The column of the target cell
* param board: The board that contains the pieces
* return: The code if the move was valid or invalid
*/
int Rook::isValidMove(int row, int column, Board *board)
{
    int i = 0;
    bool valid = true;

    // If only the column of the rook changed, check if there is anything in the movement path
    if (row == _row && column != _column)
    {
	// Get the index of the cell after the current cell of the rook
	i = _column + (column > _column ? 1 : -1);

	// Go through the cells between the old position and the new position of the rook
	for (; i != column; i += column > _column ? 1 : -1)
	{
	    // If the cell isn't empty, set it as an invalid move
	    if (!board->isEmpty(row, i))
	    {
			valid = false;
	    }
	}
    }
    // If only the row of the rook has changed, check if there is anything in the movement path
    else if (row != _row && column == _column)
    {
	// Get the index of the cell after the current cell of the rook
	i = _row + (row > _row ? 1 : -1);

	// Go through the cells between the old position and the new position of the rook
	for (; i != row; i += row > _row ? 1 : -1)
	{
	    // If the cell isn't empty, set it as an invalid move
	    if (!board->isEmpty(i, column))
	    {
			valid = false;
	    }
	}
    }
    else
    {
	    valid = false;
    }

    return valid ? PIECE_VALID_MOVE : INVALID_MOVE_OF_PIECE;
}
