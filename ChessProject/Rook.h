#pragma once
#include "Piece.h"
#include "ErrorCodes.h"

class Rook: public Piece
{
public:
	Rook(int row, int column, bool isWhite);

	virtual int isValidMove(int row, int column, Board *board);
};

